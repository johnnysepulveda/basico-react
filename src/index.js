import React from 'react';
import ReactDOM from 'react-dom';
import Aplicacion from './componentes/Aplicacion';
import './css/index.css';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(<Aplicacion/>, document.getElementById('root'));
registerServiceWorker();

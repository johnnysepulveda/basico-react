import React, {Component} from 'react'; 
import Productos from './Productos';
import Footer from './Footer';
import Header from './Header';

// esto es un class component, siempre extiende
class Aplicacion extends Component {
    constructor(props){
        super();
        this.state = {
            productos : [ ]
        };
    }

    componentDidMount(){
        this.setState({
            productos : [ 
                {nombre: 'Libro', precio: 200},
                {nombre: 'Disco de Música', precio: 100},
                {nombre: 'Instrumento Musical', precio: 600},
                {nombre: 'Producto Musical', precio: 1500},
                {nombre: 'Computador', precio: 5200}
            ]
        })
    }
    render(){
        return (
            <div>
                <Header 
                    titulo="Nuestra Tienda Virtual"
                />
                <Productos 
                    productos={this.state.productos}
                />
                <Footer />
            </div>
        )
    }
}

export default Aplicacion;
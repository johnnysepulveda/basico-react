import React, {Component} from 'react'; 
import Producto from './Producto';

// esto es un class component, siempre extiende
// accedemos a las props de las componentes de esta forma
// {console.log(this.props.productos) }

class Productos extends Component {
    render(){
        return (
            <div>
                <h2>Listado de Productos</h2>

                {/*
                Me imprime cada indix del array de objetos 
                console.log(Object.keys(this.props.productos))
                */}

                {Object.keys(this.props.productos).map(key => (
                    <Producto 
                        key={key}
                        producto={this.props.productos[key]}
                    />    
                )) }
            </div>
        )
    }
}

export default Productos;
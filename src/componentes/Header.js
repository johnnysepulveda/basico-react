import React from 'react';

// esto es un functional component recibiendo un props titulo
const Header = (props) => { 

    return(
        <header>
            <h1>{props.titulo}</h1>
        </header>
    )
}

export default Header;

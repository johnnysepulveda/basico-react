import React, {Component} from 'react'; 

// esto es un class component, siempre extiende
class Producto extends Component {
    render(){
        //const {nombre, precio} = this.props.producto;
        const nombre = this.props.producto.nombre 
        const precio = this.props.producto.precio

        return (
            <div>
                <h2>{nombre}</h2>
                <p>Precio {precio}</p>
            </div>
        )
    }
}

export default Producto;
import React from 'react'; 

// esto es un functional component
const Footer = () => {
    return (
        <footer>
            <p>Todos los derechos reservados &copy;</p>
        </footer>
    )
}

export default Footer;